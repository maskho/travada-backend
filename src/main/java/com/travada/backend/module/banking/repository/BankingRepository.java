package com.travada.backend.module.banking.repository;

import com.travada.backend.module.banking.model.Banking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface BankingRepository extends JpaRepository<Banking,Long> {
    List<Banking> findAllByUserIdOrderByCreatedAtDesc(Long id);

    List<Banking> findAllByCreatedAtBetween(Date start, Date end);

    List<Banking> findAllByOrderByIdDesc();

    List<Banking> findAllByUserIdAndCreatedAtBetween(Long id, Date start, Date end);
}
