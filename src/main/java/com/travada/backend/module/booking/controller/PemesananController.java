package com.travada.backend.module.booking.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.booking.model.Cicilan;
import com.travada.backend.module.booking.model.DTO.*;
import com.travada.backend.module.booking.model.Pemesan;
import com.travada.backend.module.booking.model.Pemesanan;
import com.travada.backend.module.booking.service.CicilanService;
import com.travada.backend.module.booking.service.PemesanService;
import com.travada.backend.module.booking.service.PemesananService;
import com.travada.backend.module.notif.model.Notif;
import com.travada.backend.module.notif.service.NotifService;
import com.travada.backend.module.trip.model.Destinasi;
import com.travada.backend.module.trip.repository.DestinasiRepository;
import com.travada.backend.module.trip.service.DestinasiService;
import com.travada.backend.module.user.model.User;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/pemesanan")
public class PemesananController {
    @Autowired
    private PemesananService pemesananService;

    @Autowired
    private CicilanService cicilanService;

    @Autowired
    private PemesanService pemesanService;

    @Autowired
    private NotifService notifService;

    @Operation(summary = "Mengambil seluruh list data pemesanan trip")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data pemesanan trip berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data pemesanan trip tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/all")
    public BaseResponse getAll() {
        return pemesananService.findAll();
    }

    @Operation(summary = "Mengambil seluruh list data pemesanan trip disetujui")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data pemesanan trip disetujui berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data pemesanan trip disetujui tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/disetujui")
    public BaseResponse getAllDiterima(){
        return pemesananService.findAllByStatusDiterima();
    }

    @Operation(summary = "Mengambil detail data pemesanan trip dengan id pemesanan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "detail data pemesanan trip berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "detail data pemesanan trip tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/detail/{idPemesanan}")
    public BaseResponse findById(@PathVariable Long idPemesanan) {
        BaseResponse baseResponse = new BaseResponse();
        DetailPemesananDTO detailPemesananDTO = new DetailPemesananDTO();

        Pemesanan pemesanan = pemesananService.findById(idPemesanan);

        List<Pemesan> pemesanList = pemesanService.getPemesan(pemesanan.getId());
        List<Cicilan> cicilanList = cicilanService.getCicilan(pemesanan.getId());

        Destinasi destinasi = pemesanan.getDestinasi();
        DestinasiDTO destinasiDTO = new DestinasiDTO();
        destinasiDTO.setId(destinasi.getId());
        destinasiDTO.setNama_trip(destinasi.getNama_trip());
        destinasiDTO.setBenua(destinasi.getBenua());
        destinasiDTO.setLokal(destinasi.getLokal());
        destinasiDTO.setGambar_list(destinasi.getGambar_list());
        destinasiDTO.setKapasitas(destinasi.getKapasitas());
        destinasiDTO.setHarga_satuan(destinasi.getHarga_satuan());
        destinasiDTO.setOverview(destinasi.getOverview());
        destinasiDTO.setDeskripsi(destinasi.getDeskripsi());
        destinasiDTO.setBerangkat(pemesanan.getBerangkat());
        destinasiDTO.setPulang(pemesanan.getPulang());
        destinasiDTO.setRencana_list(destinasi.getRencana_list());
        destinasiDTO.setFasilitas(destinasi.getFasilitas());
        destinasiDTO.setInfo_waktu_cuaca(destinasi.getInfo_waktu_cuaca());
        destinasiDTO.setInfo_persiapan(destinasi.getInfo_persiapan());
        destinasiDTO.setSyarat_ketentuan(destinasi.getSyarat_ketentuan());
        destinasiDTO.setDurasi(destinasi.getDurasi());
        destinasiDTO.setPopularitas(destinasi.getPopularitas());
        destinasiDTO.setKapasitas_terisi(destinasi.getKapasitas_terisi());

        User user = pemesanan.getUser();
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setNama(user.getNamaLengkap());
        userDTO.setNo_rekening(user.getNoRekening());
        userDTO.setNo_hp(user.getNoHp());
        userDTO.setEmail(user.getEmail());
        userDTO.setFoto_ktp(user.getFotoKtp());
        userDTO.setSelfie_ktp(user.getSelfieKtp());


        detailPemesananDTO.setPemesanan(pemesanan);
        detailPemesananDTO.setDestinasi(destinasiDTO);
        detailPemesananDTO.setPemesan(pemesanList);
        detailPemesananDTO.setCicilan(cicilanList);
        detailPemesananDTO.setUser(userDTO);

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(detailPemesananDTO);
        baseResponse.setMessage("pengambilan data pemesanan dengan id " + idPemesanan + " berhasil dilakukan");

        return baseResponse;
    }

    @GetMapping()
    public BaseResponse getAllByUserPrincipal(@CurrentUser UserPrincipal user) {
        return pemesananService.findByIdUser(user.getId());
    }

    @Operation(summary = "Mengambil seluruh list data pemesanan trip dengan id user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data pemesanan trip berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data pemesanan trip tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/{idUser}")
    public BaseResponse getAllByIdUser(@PathVariable Long idUser) {
        return pemesananService.findByIdUser(idUser);
    }

    @Operation(summary = "Mengambil data status pemesanan trip")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data status pemesanan trip berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data status pemesanan trip tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/status/all")
    public BaseResponse getAllByStatus(@RequestParam String[] status){
        BaseResponse baseResponse = new BaseResponse();
        if(status.length == 1) {
            StatusPemesanan statusPemesanan = pemesananService.findByStatus(status[0]);
            baseResponse.setData(statusPemesanan);
        }else {
            List<StatusPemesanan> statusPemesananList = new ArrayList<>();
            for(String stat: status){
                statusPemesananList.add(pemesananService.findByStatus(stat));
            }
            baseResponse.setData(statusPemesananList);
        }
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage("List pemesanan dengan status "+status+" berhasil diambil");

        return baseResponse;
    }

    @Operation(summary = "Membuat pemesanan trip")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "pemesanan trip berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "pemesanan trip tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping()
    public BaseResponse createPemesanan(@ModelAttribute CreatePemesananDTO pemesananDTO,
                                        @CurrentUser UserPrincipal user,
                                        @RequestParam MultipartFile[] foto_ktp,
                                        @RequestParam MultipartFile[] foto_paspor) {
        BaseResponse baseResponse = new BaseResponse();
        DetailPemesananDTO detailPemesananDTO = new DetailPemesananDTO();
        Pemesanan pemesanan = new Pemesanan();
        List<Pemesan> pemesanList = new ArrayList<>(pemesananDTO.getOrang());


        pemesanan.setOrang(pemesananDTO.getOrang());
        pemesanan.setStatus("Pending");
        pemesanan.setBerangkat(pemesananDTO.getBerangkat());
        pemesanan.setPulang(pemesananDTO.getPulang());
        pemesanan = pemesananService.savePemesanan(user.getId(), pemesananDTO.getIdDestinasi(), pemesanan);

        for (int i = 0; i < pemesananDTO.getOrang(); i++) {
            Pemesan pemesanData = new Pemesan();
            pemesanData.setNama(pemesananDTO.getNama().get(i));
            pemesanData.setNo_hp(pemesananDTO.getNo_hp().get(i));
            pemesanData.setEmail(pemesananDTO.getEmail().get(i));

            pemesanList.add(pemesanService.createPemesan(pemesanan.getId(), pemesanData, foto_ktp[i], foto_paspor[i]));
        }

        List<Cicilan> cicilanList = cicilanService.createCicilan(pemesananDTO.getIdDestinasi(), pemesanan.getId(), pemesananDTO.getOrang(),pemesananDTO.getBerangkat());

        detailPemesananDTO.setPemesanan(pemesanan);
        detailPemesananDTO.setCicilan(cicilanList);
        detailPemesananDTO.setPemesan(pemesanList);

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(detailPemesananDTO);
        baseResponse.setMessage("pemesanan telah dibuat");
        return baseResponse;
    }

    @Operation(summary = "Membuat pemesanan trip base64")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "pemesanan trip berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "pemesanan trip tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/base64")
    public BaseResponse createPemesananBase64(@RequestBody CreatePemesananDTO pemesananDTO, @CurrentUser UserPrincipal user) {
        BaseResponse baseResponse = new BaseResponse();
        DetailPemesananDTO detailPemesananDTO = new DetailPemesananDTO();
        Pemesanan pemesanan = new Pemesanan();
        List<Pemesan> pemesanList = new ArrayList<>(pemesananDTO.getOrang());


        pemesanan.setOrang(pemesananDTO.getOrang());
        pemesanan.setStatus("Pending");
        pemesanan.setBerangkat(pemesananDTO.getBerangkat());
        pemesanan.setPulang(pemesananDTO.getPulang());
        pemesanan = pemesananService.savePemesanan(user.getId(), pemesananDTO.getIdDestinasi(), pemesanan);


        for (int i = 0; i < pemesananDTO.getOrang(); i++) {
            Pemesan pemesanData = new Pemesan();
            pemesanData.setNama(pemesananDTO.getNama().get(i));
            pemesanData.setNo_hp(pemesananDTO.getNo_hp().get(i));
            pemesanData.setEmail(pemesananDTO.getEmail().get(i));

            pemesanList.add(pemesanService.createPemesanBase64(pemesanan.getId(), pemesanData, pemesananDTO.getKtp().get(i), pemesananDTO.getPaspor().get(i)));
        }

        List<Cicilan> cicilanList = cicilanService.createCicilan(pemesananDTO.getIdDestinasi(), pemesanan.getId(), pemesananDTO.getOrang(),pemesananDTO.getBerangkat());

        detailPemesananDTO.setPemesanan(pemesanan);
        detailPemesananDTO.setCicilan(cicilanList);
        detailPemesananDTO.setPemesan(pemesanList);

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(detailPemesananDTO);
        baseResponse.setMessage("pemesanan telah dibuat");
        return baseResponse;
    }

    @Operation(summary = "Membuat pemesanan trip dengan id user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "pemesanan trip berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "pemesanan trip tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/{idUser}")
    public BaseResponse createPemesananIdUser(@ModelAttribute CreatePemesananDTO pemesananDTO, @PathVariable Long idUser, @RequestParam MultipartFile[] foto_ktp, @RequestParam MultipartFile[] foto_paspor) {
        BaseResponse baseResponse = new BaseResponse();
        DetailPemesananDTO detailPemesananDTO = new DetailPemesananDTO();
        Pemesanan pemesanan = new Pemesanan();
        List<Pemesan> pemesanList = new ArrayList<>(pemesananDTO.getOrang());


        pemesanan.setOrang(pemesananDTO.getOrang());
        pemesanan.setStatus("Pending");
        pemesanan.setBerangkat(pemesananDTO.getBerangkat());
        pemesanan.setPulang(pemesananDTO.getPulang());
        pemesanan = pemesananService.savePemesanan(idUser, pemesananDTO.getIdDestinasi(), pemesanan);

        for (int i = 0; i < pemesananDTO.getOrang(); i++) {
            Pemesan pemesanData = new Pemesan();
            pemesanData.setNama(pemesananDTO.getNama().get(i));
            pemesanData.setNo_hp(pemesananDTO.getNo_hp().get(i));
            pemesanData.setEmail(pemesananDTO.getEmail().get(i));

            pemesanList.add(pemesanService.createPemesan(pemesanan.getId(), pemesanData, foto_ktp[i], foto_paspor[i]));
        }

        List<Cicilan> cicilanList = cicilanService.createCicilan(pemesananDTO.getIdDestinasi(), pemesanan.getId(), pemesananDTO.getOrang(),pemesananDTO.getBerangkat());

        detailPemesananDTO.setPemesanan(pemesanan);
        detailPemesananDTO.setCicilan(cicilanList);
        detailPemesananDTO.setPemesan(pemesanList);

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(detailPemesananDTO);
        baseResponse.setMessage("pemesanan telah dibuat");
        return baseResponse;
    }

    @Operation(summary = "Membuat pemesanan trip dengan id user base64")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "pemesanan trip berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "pemesanan trip tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/base64/{idUser}")
    public BaseResponse createPemesananBase64(@RequestBody CreatePemesananDTO pemesananDTO, @PathVariable Long idUser) {
        BaseResponse baseResponse = new BaseResponse();
        DetailPemesananDTO detailPemesananDTO = new DetailPemesananDTO();
        Pemesanan pemesanan = new Pemesanan();
        List<Pemesan> pemesanList = new ArrayList<>(pemesananDTO.getOrang());


        pemesanan.setOrang(pemesananDTO.getOrang());
        pemesanan.setStatus("Pending");
        pemesanan.setBerangkat(pemesananDTO.getBerangkat());
        pemesanan.setPulang(pemesananDTO.getPulang());
        pemesanan = pemesananService.savePemesanan(idUser, pemesananDTO.getIdDestinasi(), pemesanan);


        for (int i = 0; i < pemesananDTO.getOrang(); i++) {
            Pemesan pemesanData = new Pemesan();
            pemesanData.setNama(pemesananDTO.getNama().get(i));
            pemesanData.setNo_hp(pemesananDTO.getNo_hp().get(i));
            pemesanData.setEmail(pemesananDTO.getEmail().get(i));

            pemesanList.add(pemesanService.createPemesanBase64(pemesanan.getId(), pemesanData, pemesananDTO.getKtp().get(i), pemesananDTO.getPaspor().get(i)));
        }

        List<Cicilan> cicilanList = cicilanService.createCicilan(pemesananDTO.getIdDestinasi(), pemesanan.getId(), pemesananDTO.getOrang(),pemesananDTO.getBerangkat());

        detailPemesananDTO.setPemesanan(pemesanan);
        detailPemesananDTO.setCicilan(cicilanList);
        detailPemesananDTO.setPemesan(pemesanList);

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(detailPemesananDTO);
        baseResponse.setMessage("pemesanan telah dibuat");
        return baseResponse;
    }

    @Operation(summary = "Memperbarui status pemesanan trip")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "update pemesanan trip berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "update pemesanan trip tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PutMapping("/{id}")
    public BaseResponse updateStatusPemesanan(@PathVariable Long id, @RequestParam String status) {
        BaseResponse baseResponse = new BaseResponse();
        Pemesanan pemesanan = pemesananService.updateStatusById(id, status);
        Notif notif = new Notif();
        notif.setJudul("Verifikasi Trava Plan");
        notif.setJenis("Travaplan");
        notif.setUser(pemesanan.getUser());
        notif.setPemesanan(pemesanan);
        notif.setDestinasi(pemesanan.getDestinasi());
        notif.setTerbaca(false);
        if(status.contains("tolak")){
            notif.setPesan("Pengajuan Trava Plan "+pemesanan.getDestinasi().getNama_trip()+" ditolak oleh Travada. Info lebih lanjut hubungi CS Travada.");
        }else{
            notif.setPesan("Pengajuan Trava Plan "+pemesanan.getDestinasi().getNama_trip()+" telah disetujui oleh Travada. Lakukan pembayaran cicilan sebelum tanggal jatuh tempo tiba.");
        }
        notifService.createNotif(notif);

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(pemesanan);
        baseResponse.setMessage("status pemesanan dengan id " + id + " berhasil diupdate");
        return baseResponse;
    }

    @Operation(summary = "Menghapus pemesanan trip")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "pemesanan trip berhasil dihapus", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "pemesanan trip tidak berhasil dihapus", content = {@Content(mediaType = "application/json")})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePemesanan(@PathVariable Long id) {
        return pemesananService.dropById(id);
    }
}
