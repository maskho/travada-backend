package com.travada.backend.module.booking.model.DTO;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String nama;
    private String no_rekening;
//    private String alamat;
    private String no_hp;
    private String email;
    private String foto_ktp;
    private String selfie_ktp;
}
