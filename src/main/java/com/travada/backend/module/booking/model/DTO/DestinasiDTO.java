package com.travada.backend.module.booking.model.DTO;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
public class DestinasiDTO {
    private Long id;
    private String nama_trip;
    private String benua;
    private Boolean lokal;
    private List<String> gambar_list;
    private int kapasitas;
    private Long harga_satuan;
    private String overview;
    private String deskripsi;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate berangkat;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate pulang;
    private List<String> rencana_list;
    private Set<String> fasilitas;
    private String info_waktu_cuaca;
    private String info_persiapan;
    private String syarat_ketentuan;
    private int durasi;
    private int popularitas;
    private int kapasitas_terisi;
}
