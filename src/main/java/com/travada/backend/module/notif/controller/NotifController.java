package com.travada.backend.module.notif.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.notif.service.NotifService;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/notif")
@RestController
public class NotifController {
    @Autowired
    private NotifService notifService;

    @Operation(summary = "Mengambil seluruh data notifikasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data notifikasi berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data notifikasi tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping()
    public BaseResponse getAllNotif(@CurrentUser UserPrincipal user){
        return notifService.getAll(user.getId());
    }

    @Operation(summary = "Mengambil seluruh data notifikasi by id user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data notifikasi user berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data notifikasi user tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/{idUser}")
    public BaseResponse getAllNotif(@PathVariable Long idUser){
        return notifService.getAll(idUser);
    }

    @Operation(summary = "Mengambil seluruh data notifikasi by id notifikasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data notifikasi berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data notifikasi tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/detail/{idNotif}")
    public BaseResponse getDetailNotif(@PathVariable Long idNotif){
        return notifService.getById(idNotif);
    }

    @Operation(summary = "Menghapus data notifkasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data notifikasi berhasil dihapus", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data notifikasi tidak berhasil dihapus", content = {@Content(mediaType = "application/json")})
    })
    @DeleteMapping("/{idNotif}")
    public ResponseEntity<?> dropNotifById(@PathVariable Long idNotif){
        return notifService.dropNotif(idNotif);
    }
}
