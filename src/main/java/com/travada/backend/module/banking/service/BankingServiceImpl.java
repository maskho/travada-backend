package com.travada.backend.module.banking.service;

import com.travada.backend.bank.chache.RedisTokenRepository;
import com.travada.backend.bank.request.RequestService;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.exception.DataNotFoundException;
import com.travada.backend.exception.ResourceNotFoundException;
import com.travada.backend.module.banking.model.Banking;
import com.travada.backend.module.banking.model.Dana;
import com.travada.backend.module.banking.model.TransaksiDTO;
import com.travada.backend.module.banking.repository.BankingRepository;
import com.travada.backend.module.banking.repository.DanaRepository;
import com.travada.backend.module.booking.model.Cicilan;
import com.travada.backend.module.booking.repository.CicilanRepository;
import com.travada.backend.module.notif.model.Notif;
import com.travada.backend.module.notif.service.NotifService;
import com.travada.backend.module.tabungan.model.Tabungan;
import com.travada.backend.module.tabungan.repository.TabunganRepository;
import com.travada.backend.module.transaksi.model.Saldo;
import com.travada.backend.module.user.model.User;
import com.travada.backend.module.user.repository.UserRepository;
import com.travada.backend.utils.BaseResponse;
import com.travada.backend.utils.crypto.EncoderHelper;
import org.cloudinary.json.JSONObject;
import org.modelmapper.internal.asm.tree.TryCatchBlockNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.representer.BaseRepresenter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.travada.backend.bank.chache.RedisConfig.redisTemplate;

@Service("bankingServiceImpl")
public class BankingServiceImpl implements BankingService {

    @Autowired
    private BankingRepository bankingRepository;
    @Autowired
    private DanaRepository danaRepository;
    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RequestService requestService;
    @Autowired
    private EncoderHelper encoderHelper;
    @Autowired
    private TabunganRepository tabunganRepository;

    RestTemplate restTemplate = new RestTemplate();
    RedisTokenRepository tokenRepository = new RedisTokenRepository(redisTemplate());

    String tokenTP = tokenRepository.get("authTP").getToken();

    public String rekeningTravada = "08157642696";

    @Override
    public Banking topUpSaldo(UserPrincipal userPrincipal, String rekening, String pin, String nominal) {
        String currentUsername = userPrincipal.getUsername();
        User user = userRepository.findByUsername(currentUsername)
                .orElseThrow(ResourceNotFoundException::new);

        Banking banking = new Banking();

        banking.setRekening_asal(rekening);
        banking.setRekening_tujuan(user.getNoRekening());
        banking.setBank_asal("Binar");
        banking.setBank_tujuan("Travada");
        banking.setNama_asal(user.getNamaLengkap());
        banking.setNama_tujuan("Saldo Aktif");
        banking.setUser(user);
        banking.setNominal(nominal);
        banking.setCatatan("Top up saldo");
        bankingRepository.save(banking);
        BigDecimal jumlahDana = new BigDecimal(nominal);

        try {
            Dana dana = danaRepository.findByUserId(user.getId());
            dana.setDana(dana.getDana().add(jumlahDana));
            danaRepository.save(dana);
        } catch (NullPointerException e) {
            Dana dana = new Dana();
            dana.setUser(user);
            dana.setDana(BigDecimal.valueOf(0));
            dana.setDana(dana.getDana().add(jumlahDana));
            danaRepository.save(dana);
        }


        String hasil = requestService.requestTransfer(rekening, rekeningTravada, nominal);
        String rawData = hasil + ":" + encoderHelper.encryptBase64(pin);
        String preEncryptData = encoderHelper.encryptAES256(rawData, "$2a$10$3Ap9dKoJekGXNSLYIZ6AROg66Ncrjw7gz427r5z89NyGtljMs9iu.");
        String preSendData = preEncryptData + ":JDJhJDEwJDNBcDlkS29KZWtHWE5TTFlJWjZBUk9nbFBsTWFibnFsWW44QlU0cy5ibW9DS3RVaHV0ZkZ5";
        String data = encoderHelper.encryptBase64(preSendData);

        // create request body
        JSONObject request = new JSONObject();
        request.put("data", data);
//
        String urlString = "http://api.bank.binar.ariefdfaltah.com/bank/account/transfer";
//
        // set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(tokenTP);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        // send request and parse result
        ResponseEntity<String> loginResponse = restTemplate
                .exchange(urlString, HttpMethod.POST, entity, String.class);


        return banking;
    }

    @Override
    public Banking transferAntarTravada(UserPrincipal userPrincipal, String pin, String rekeningTujuan, String nominal, String catatan) {
        String currentUsername = userPrincipal.getUsername();
        User user = userRepository.findByUsername(currentUsername)
                .orElseThrow(ResourceNotFoundException::new);
        User userTujuan = userRepository.findBynoRekening(rekeningTujuan);
        if (userTujuan == null) {
            return null;
        }

        Banking banking = new Banking();
        Banking bankingTujuan = new Banking();

        banking.setRekening_asal(user.getNoRekening());
        bankingTujuan.setRekening_asal(user.getNoRekening());

        banking.setRekening_tujuan(rekeningTujuan);
        banking.setBank_asal("Travada");
        banking.setBank_tujuan("Travada");
        banking.setNama_asal(user.getNamaLengkap());
        banking.setNama_tujuan(userTujuan.getNamaLengkap());
        banking.setCatatan(catatan);
        banking.setUser(user);
        banking.setNominal("-" + nominal);
        bankingRepository.save(banking);
        BigDecimal jumlahDana = new BigDecimal(nominal);

        try {
            Dana dana = danaRepository.findByUserId(user.getId());
            BigDecimal danaPengirim = dana.getDana();
            dana.setDana(danaPengirim.subtract(jumlahDana));
            Dana danaTujuan = danaRepository.findByUserId(userTujuan.getId());
            BigDecimal danaPenerima = danaTujuan.getDana();
            danaTujuan.setDana(danaPenerima.add(jumlahDana));
            danaRepository.save(danaTujuan);
            danaRepository.save(dana);
        } catch (NullPointerException e) {
            return null;
        }

        bankingTujuan.setRekening_tujuan(rekeningTujuan);
        bankingTujuan.setBank_asal("Travada");
        bankingTujuan.setBank_tujuan("Travada");
        bankingTujuan.setNama_asal(user.getNamaLengkap());
        bankingTujuan.setNama_tujuan(userTujuan.getNamaLengkap());
        bankingTujuan.setCatatan(catatan);
        bankingTujuan.setUser(user);
        bankingTujuan.setNominal(nominal);
        bankingTujuan.setUser(userTujuan);
        bankingTujuan.setNominal(nominal);
        bankingRepository.save(bankingTujuan);

        return banking;
    }

    @Override
    public Banking transferAntarBinar(UserPrincipal userPrincipal, String rekeningAsal, String pin, String rekeningTujuan, String nominal) {
        return null;
    }

    @Override
    public BaseResponse mutasiAll(UserPrincipal userPrincipal) {
        BaseResponse baseResponse = new BaseResponse();
        List<Banking> bankingList = bankingRepository.findAllByUserIdOrderByCreatedAtDesc(userPrincipal.getId());

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(bankingList);
        baseResponse.setMessage("Pengambilan data mutasi user dengan id " + userPrincipal.getId() + " telah berhasil");


        return baseResponse;
    }

    @Override
    public BaseResponse mutasiByDate(UserPrincipal userPrincipal, LocalDate first, LocalDate second) {
        BaseResponse baseResponse = new BaseResponse();
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Date awal = Date.from(first.atStartOfDay(defaultZoneId).toInstant());
        Date akhir = Date.from(second.atStartOfDay(defaultZoneId).toInstant());
        List<Banking> bankingList = bankingRepository.findAllByUserIdAndCreatedAtBetween(userPrincipal.getId(), awal, akhir);
        baseResponse.setMessage("Pengambilan data berhasil");
        baseResponse.setData(bankingList);
        baseResponse.setStatus(HttpStatus.OK);
        return baseResponse;
    }

    @Override
    public BaseResponse riwayatAll() {
        BaseResponse baseResponse = new BaseResponse();
        List<Banking> bankingList = bankingRepository.findAll();

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(bankingList);
        baseResponse.setMessage("Pengambilan data mutasi user telah berhasil");

        return baseResponse;

    }

    @Override
    public BaseResponse mutasiById(Long id) {
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(bankingRepository.findById(id));
        baseResponse.setMessage("Pengambilan data mutasi user telah berhasil");

        return baseResponse;
    }

    @Override
    public Banking tarikSaldo(UserPrincipal userPrincipal, String rekening, String pin, String nominal) {
        String currentUsername = userPrincipal.getUsername();
        User user = userRepository.findByUsername(currentUsername)
                .orElseThrow(ResourceNotFoundException::new);

        Banking banking = new Banking();

        banking.setRekening_asal(user.getNoRekening());
        banking.setRekening_tujuan(rekening);
        banking.setBank_asal("Travada");
        banking.setBank_tujuan("Binar");
        banking.setNama_asal("Saldo Aktif");
        banking.setNama_tujuan(user.getNamaLengkap());
        banking.setUser(user);
        banking.setNominal("-" + nominal);
        bankingRepository.save(banking);
        BigDecimal jumlahDana = new BigDecimal(nominal);

        try {
            Dana dana = danaRepository.findByUserId(user.getId());
            dana.setDana(dana.getDana().subtract(jumlahDana));
            danaRepository.save(dana);
        } catch (NullPointerException e) {
            Dana dana = new Dana();
            dana.setUser(user);
            dana.setDana(BigDecimal.valueOf(0));
            dana.setDana(dana.getDana().subtract(jumlahDana));
            danaRepository.save(dana);
        }


        String hasil = requestService.requestTransfer(rekeningTravada, rekening, nominal);
        String rawData = hasil + ":" + encoderHelper.encryptBase64("642696");
        String preEncryptData = encoderHelper.encryptAES256(rawData, "$2a$10$3Ap9dKoJekGXNSLYIZ6AROg66Ncrjw7gz427r5z89NyGtljMs9iu.");
        String preSendData = preEncryptData + ":JDJhJDEwJDNBcDlkS29KZWtHWE5TTFlJWjZBUk9nbFBsTWFibnFsWW44QlU0cy5ibW9DS3RVaHV0ZkZ5";
        String data = encoderHelper.encryptBase64(preSendData);

        // create request body
        JSONObject request = new JSONObject();
        request.put("data", data);
//
        String urlString = "http://api.bank.binar.ariefdfaltah.com/bank/account/transfer";
//
        // set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(tokenTP);
        HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

        // send request and parse result
        ResponseEntity<String> loginResponse = restTemplate
                .exchange(urlString, HttpMethod.POST, entity, String.class);
        //if (loginResponse.getStatusCode() == HttpStatus.CREATED) {
//            JSONObject userJson = new JSONObject(loginResponse.getBody());
//            JSONObject result = userJson.getJSONObject("message");
//            String finalResult = result.get("request_id").toString();

//
//        banking.setNama_asal(user.getNamaLengkap());
//        banking.setNama_tujuan("Saldo Aktif");
//        banking.setRekening_asal(user.getNoRekening());
//        banking.setRekening_tujuan(user.getNoRekening());
        return banking;
    }

    @Override
    public BaseResponse transaksiAll() {
        BaseResponse baseResponse = new BaseResponse();
        List<Banking> bankingList = bankingRepository.findAllByOrderByIdDesc();
        List<Tabungan> tabunganList = tabunganRepository.findAll();
        List<TransaksiDTO> transaksiList = new ArrayList<>();


        for (Banking banking : bankingList) {
            TransaksiDTO transaksi = new TransaksiDTO();
            transaksi.setId(banking.getId());
            transaksi.setNama_nasabah(banking.getUser().getNamaLengkap());
            transaksi.setTanggal(banking.getUpdatedAt());
            transaksi.setJumlah(banking.getNominal());

            if(banking.getNama_tujuan().contains("Trava Plan")){
                transaksi.setDeskripsi(banking.getCatatan());
                transaksi.setJenis_transaksi("Trava Plan");
            }
            else if (banking.getRekening_tujuan() == rekeningTravada) {
                transaksi.setDeskripsi(banking.getCatatan());
                transaksi.setJenis_transaksi("Top Up");
            }
            else if(banking.getRekening_asal()==rekeningTravada){
                transaksi.setDeskripsi("Tarik saldo Travada");
                transaksi.setJenis_transaksi("Tarik Saldo");
            }
            else{
                transaksi.setDeskripsi("Transfer ke "+banking.getNama_tujuan());
                transaksi.setJenis_transaksi("Transfer");
            }
            transaksiList.add(transaksi);
        }
//        for(Cicilan cicilan:cicilanList){
//            if(cicilan.getStatus().contains("Dibayar")){
//                TransaksiDTO transaksi = new TransaksiDTO();
//                transaksi.setId(cicilan.getId());
//                transaksi.setNama_nasabah(cicilan.getPemesanan().getUser().getNamaLengkap());
//                transaksi.setTanggal(cicilan.getPemesanan().getUpdatedAt());
//                transaksi.setJumlah(String.valueOf(cicilan.getJumlah()));
//                transaksi.setDeskripsi("Bayar cicilan "+cicilan.getPemesanan().getDestinasi().getNama_trip());
//                transaksi.setJenis_transaksi("Travaplan");
//                transaksiList.add(transaksi);
//            }
//        }

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(transaksiList);
        baseResponse.setMessage("List transaksi berhasil diambil");

        return baseResponse;
    }
}
