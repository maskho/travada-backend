package com.travada.backend.module.banking.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.banking.model.Banking;
import com.travada.backend.module.banking.model.TransaksikuDTO;
import com.travada.backend.module.banking.model.TransferkuDTO;
import com.travada.backend.module.banking.service.BankingService;
import com.travada.backend.module.user.model.User;
import com.travada.backend.module.user.service.UserService;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.lang.UsesSunHttpServer;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/banking")
public class BankingController {
    @Autowired
    private BankingService bankingService;

    @Autowired
    private UserService userService;

    @Operation(summary = "Top up saldo")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "top up saldo berhasil",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "top up tidak berhasil",
                    content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/topup")
    public BaseResponse topUpSaldo(@CurrentUser UserPrincipal user, @RequestBody TransaksikuDTO topup) {
        BaseResponse baseResponse = new BaseResponse();
        String rekening = topup.getRekening();
        String pin = topup.getPin();
        String nominal = topup.getNominal();
        baseResponse.setData(bankingService.topUpSaldo(user, rekening, pin, nominal));
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage("Top Up saldo sejumlah " + nominal + " berhasil dilakukan");
        return baseResponse;
    }

    @Operation(summary = "Tarik saldo dari Travada ke Bank Binar")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "tarik saldo berhasil",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "tarik saldo tidak berhasil",
                    content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/tarik")
    public BaseResponse tarikSaldo(@CurrentUser UserPrincipal user, @RequestBody TransaksikuDTO tarik) {
        BaseResponse baseResponse = new BaseResponse();
        String rekening = tarik.getRekening();
        String pin = tarik.getPin();
        String nominal = tarik.getNominal();
        baseResponse.setData(bankingService.tarikSaldo(user, rekening, pin, nominal));
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage("Tarik saldo sejumlah -" + nominal + " berhasil dilakukan");
        return baseResponse;
    }

    @Operation(summary = "Transfer antar akun Travada")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "transfer berhasil",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "transfer tidak berhasil",
                    content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/transfertravada")
    public BaseResponse transferAntarAkunTravada(@CurrentUser UserPrincipal userPrincipal, @RequestBody TransferkuDTO transfer) {
        BaseResponse baseResponse = new BaseResponse();
        String rekeningTujuan = transfer.getRekeningTujuan();
        String nominal = transfer.getNominal();
        String catatan = transfer.getCatatan();
        String pin = transfer.getPin();
        Banking banking = bankingService.transferAntarTravada(userPrincipal, pin, rekeningTujuan, nominal, catatan);
        if(banking==null){
            baseResponse.setStatus(HttpStatus.NOT_FOUND);
            baseResponse.setMessage("Transfer saldo antar akun Travada gagal, data rekening tidak ditemukan");
        }else{
        baseResponse.setData(banking);
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setMessage("Transfer saldo antar akun Travada sejumlah " + nominal + " berhasil dilakukan");}
        return baseResponse;
    }
    @Operation(summary = "Mengambil seluruh list data mutasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/all")
    public BaseResponse mutasiUser(@CurrentUser UserPrincipal user) {
        return bankingService.mutasiAll(user);
    }

    @Operation(summary = "Mengambil data mutasi menggunakan filter berdasarkan tanggal")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/filter")
    public BaseResponse mutasiFilterTgl(@CurrentUser UserPrincipal user, @RequestParam(value = "awal") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate awal, @RequestParam(value = "akhir") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate akhir){
        return bankingService.mutasiByDate(user,awal,akhir);
    }

    @Operation(summary = "Mengambil data mutasi menggunakan filter berdasarkan minggu")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/filter/mingguan")
    public BaseResponse mutasiMinggu(@CurrentUser UserPrincipal user){
        LocalDate akhir = LocalDate.now();
        LocalDate awal = LocalDate.now().minusWeeks(1);
//        return bankingService.mutasiByDate(user,awal,akhir);
        return bankingService.mutasiAll(user);
    }

    @Operation(summary = "Mengambil data mutasi menggunakan filter berdasarkan bulan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/filter/bulanan")
    public BaseResponse mutasiBulan(@CurrentUser UserPrincipal user){
        LocalDate akhir = LocalDate.now();
        LocalDate awal = LocalDate.now().minusMonths(1);
//        return bankingService.mutasiByDate(user,awal,akhir);
        return bankingService.mutasiAll(user);
    }

    @Operation(summary = "Mengambil data mutasi menggunakan filter berdasarkan tahun")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/filter/tahunan")
    public BaseResponse mutasiTahun(@CurrentUser UserPrincipal user){
        LocalDate akhir = LocalDate.now();
        LocalDate awal = LocalDate.now().minusYears(1);
//        return bankingService.mutasiByDate(user,awal,akhir);
        return bankingService.mutasiAll(user);
    }

    @Operation(summary = "Mengambil data mutasi by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/mutasi/{id}")
    public BaseResponse mutasiUser(@PathVariable Long id) {
        return bankingService.mutasiById(id);
    }

    @Operation(summary = "Mengambil seluruh riwayat data mutasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data riwayat mutasi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data riwayat mutasi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/riwayat/all")
    public BaseResponse riwayatAll() {
        return bankingService.riwayatAll();
    }
    @Operation(summary = "Mengambil seluruh riwayat data transaksi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "data riwayat transaksi berhasil dimuat",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "data riwayat transaksi tidak berhasil dimuat",
                    content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/transaksi/all")
    public BaseResponse transaksiAll(){
        return bankingService.transaksiAll();
    }
}
