package com.travada.backend.module.banking.repository;

import com.travada.backend.module.banking.model.Dana;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DanaRepository extends JpaRepository<Dana,Long> {
    Dana findByUserId(Long id);
}
