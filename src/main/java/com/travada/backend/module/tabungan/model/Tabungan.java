package com.travada.backend.module.tabungan.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.travada.backend.module.user.model.User;
import com.travada.backend.utils.AuditModel;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tabungan")
@Data
public class Tabungan extends AuditModel {
    @Id @GeneratedValue(generator = "generator_tabungan")
    @SequenceGenerator(
            name = "generator_tabungan",
            sequenceName = "sequence_tabungan",
            initialValue = 5000
    )
    private Long id;

    @Column(name = "tujuan")
    private String tujuan;

    @Column(name = "jumlah_tabungan")
    private Long jumlah_tabungan;

    @Column(name = "gambar_tabungan")
    private  String gambar_tabungan;

    @Column(name = "target")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate target;

    @Column(name = "setoran_awal")
    private Long setoran_awal;

    private Boolean autodebet;

    @Column(name = "periode")
    private String periode;

    @Column(name = "jumlah_setoran")
    private Long jumlah_setoran;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "nama", joinColumns = @JoinColumn(name = "id_tabungan"))
    private List<String> nama;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "rekening", joinColumns = @JoinColumn(name = "id_tabungan"))
    private List<String> rekening;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user;
//    private int jumlah_orang;
    private String status;

    private Long terkumpul;

}

