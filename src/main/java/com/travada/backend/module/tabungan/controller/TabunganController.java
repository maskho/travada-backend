package com.travada.backend.module.tabungan.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.tabungan.model.Tabungan;
import com.travada.backend.module.tabungan.model.TemanDTO;
import com.travada.backend.module.tabungan.service.TabunganService;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

@RestController
@RequestMapping("/tabungan")
public class TabunganController {
    @Autowired
    private TabunganService tabunganService;

    @Operation(summary = "Membuat tabungan Trava Save")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "tabungan trava save berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "tabungan trava save tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping()
    public BaseResponse createSave(@CurrentUser UserPrincipal userPrincipal, @ModelAttribute Tabungan tabungan,
                                   @RequestParam MultipartFile foto){
        return tabunganService.saveTabungan(tabungan, userPrincipal, foto);
    }
    @PostMapping("/base64")
    public BaseResponse createSaveBase64(@CurrentUser UserPrincipal userPrincipal, @RequestBody Tabungan tabungan){

        return tabunganService.saveTabungan64(tabungan, userPrincipal);
    }

    @Operation(summary = "Mengambil data teman yang diinvite")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/teman")
    public TemanDTO inviteTeman(@RequestParam String rekening){
        return tabunganService.tambahTeman(rekening);
    }

    @Operation(summary = "Mengambil seluruh data tabungan Trava Save")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data tabungan Trava Save berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data tabungan Trava Save tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/all")
    public BaseResponse getAll() {
        return tabunganService.findAll();
    }

    @Operation(summary = "Mengambil seluruh data user Trava Save")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data user Trava Save berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data user Trava Save tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/user/all")
    public BaseResponse getUserAll(@CurrentUser UserPrincipal userPrincipal){return tabunganService.findUserAll(userPrincipal);}

    @Operation(summary = "Mengambil data user Trava Save by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data user Trava Save berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data user Trava Save tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/{id}")
    public BaseResponse getById(@PathVariable Long id) {
        return tabunganService.findById(id);
    }

    @Operation(summary = "Membayar tabungan via metode manual (top up)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @PutMapping("/bayar/{idTabungan}")
    public BaseResponse bayarTabungan(@PathVariable Long idTabungan){
        return tabunganService.bayarManual(idTabungan);
    }

    @Operation(summary = "Menghapus data Tabungan Trava Save")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data berhasil dihapus", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data tidak berhasil dihapus", content = {@Content(mediaType = "application/json")})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> dropSave(@PathVariable Long id){
        return tabunganService.dropSave(id);
    }
}
