package com.travada.backend.module.banking.service;

import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.banking.model.Banking;
import com.travada.backend.module.user.model.User;
import com.travada.backend.utils.BaseResponse;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public interface BankingService {
    Banking topUpSaldo(UserPrincipal userPrincipal, String rekening, String pin, String nominal);

    Banking transferAntarTravada(UserPrincipal userPrincipal, String pin, String rekeningTujuan, String nominal, String catatan);

    Banking transferAntarBinar(UserPrincipal userPrincipal, String rekeningAsal, String pin, String rekeningTujuan, String nominal);

    BaseResponse mutasiAll(UserPrincipal userPrincipal);

    BaseResponse mutasiByDate(UserPrincipal userPrincipal, LocalDate awal, LocalDate akhir);

    BaseResponse riwayatAll();

    BaseResponse mutasiById(Long id);

    Banking tarikSaldo(UserPrincipal userPrincipal, String rekening, String pin, String nominal);

    BaseResponse transaksiAll();
}
