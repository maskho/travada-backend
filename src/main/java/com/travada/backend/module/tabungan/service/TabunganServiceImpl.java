package com.travada.backend.module.tabungan.service;

import com.cloudinary.utils.ObjectUtils;
import com.travada.backend.config.CloudinaryConfig;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.exception.DataNotFoundException;
import com.travada.backend.module.banking.model.Dana;
import com.travada.backend.module.banking.repository.DanaRepository;
import com.travada.backend.module.notif.model.Notif;
import com.travada.backend.module.notif.service.NotifService;
import com.travada.backend.module.tabungan.model.DaftarTabunganDTO;
import com.travada.backend.module.tabungan.model.Tabungan;
//import com.travada.backend.module.tabungan.repository.PenabungRepository;
import com.travada.backend.module.tabungan.model.TemanDTO;
import com.travada.backend.module.tabungan.repository.TabunganRepository;
import com.travada.backend.module.user.model.User;
import com.travada.backend.module.user.repository.UserRepository;
import com.travada.backend.utils.BaseResponse;
import com.travada.backend.utils.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Service("tabunganServiceImpl")
public class TabunganServiceImpl implements TabunganService {
    @Autowired
    private TabunganRepository tabunganRepository;

    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;

//    @Autowired
//    private PenabungRepository penabungRepository;

    @Autowired
    private CloudinaryConfig cloudinaryConfig;

    @Autowired
    private DanaRepository danaRepository;

    @Autowired
    private NotifService notifService;


    @Transactional
    public BaseResponse saveTabungan(Tabungan tabungan, UserPrincipal userPrincipal,
                                     MultipartFile gambar_tabungan) {
        BaseResponse baseResponse = new BaseResponse();
        try {
            Map uploadResult = cloudinaryConfig.upload(gambar_tabungan.getBytes(),
                    ObjectUtils.asMap("resourcetype", "auto"));

            tabungan.setGambar_tabungan(uploadResult.get("url").toString());

        } catch (IOException e) {
            return new BaseResponse
                    (HttpStatus.BAD_REQUEST,
                            null,
                            "Upload foto gagal");
        }
        User user = userRepository.findById(userPrincipal.getId())
                .orElseThrow();
        tabungan.setUser(user);

        Period periode = Period.between(tabungan.getTarget(), LocalDate.now());
        Long bayar = (tabungan.getJumlah_tabungan() - tabungan.getSetoran_awal() * (tabungan.getRekening().size()));
        Long hari = ChronoUnit.DAYS.between(tabungan.getTarget(), LocalDate.now());
        int cicil = tabungan.getRekening().size();
        if (tabungan.getPeriode().toLowerCase().contains("harian")) {
            tabungan.setJumlah_setoran(bayar / ((-1 * hari) * cicil));
        } else if (tabungan.getPeriode().toLowerCase().contains("mingguan")) {
            tabungan.setJumlah_setoran(bayar / (((-1 * hari) / 7) * cicil));
        } else {
            tabungan.setJumlah_setoran(bayar / (((-1 * hari) / 30) * cicil));
        }
        Dana dana = danaRepository.findByUserId(user.getId());
        if (tabungan.getAutodebet()) {
            dana.setDana(dana.getDana().subtract(BigDecimal.valueOf(tabungan.getSetoran_awal())));
            tabungan.setTerkumpul(tabungan.getSetoran_awal());
            danaRepository.save(dana);
        } else {
            tabungan.setTerkumpul((long) 0);
        }
//        tabunganRepository.save(tabungan);

        if (tabungan.getRekening() != null) {
            for (String rek : tabungan.getRekening()) {
                Tabungan tabunganTeman = new Tabungan();
                User userTeman = userRepository.findBynoRekening(rek);
                if (userTeman != null) {
                    tabunganTeman.setTujuan(tabungan.getTujuan());
                    tabunganTeman.setJumlah_tabungan(tabungan.getJumlah_tabungan());
                    tabunganTeman.setGambar_tabungan(tabungan.getGambar_tabungan());
                    tabunganTeman.setTarget(tabungan.getTarget());
                    tabunganTeman.setSetoran_awal(tabungan.getSetoran_awal());
                    tabunganTeman.setAutodebet(tabungan.getAutodebet());
                    tabunganTeman.setPeriode(tabungan.getPeriode());
                    tabunganTeman.setJumlah_setoran(tabungan.getJumlah_setoran());
                    tabunganTeman.setTerkumpul(tabungan.getTerkumpul());
                    tabunganTeman.setUser(userTeman);
                    tabunganRepository.save(tabunganTeman);

                    if (rek != tabungan.getRekening().get(0)) {
                        Notif notif = new Notif();
                        notif.setJudul("Permintaan Gabung");
                        notif.setJenis("Travasave");
                        notif.setPesan(user.getNamaLengkap() + " mengundang kamu ke dalam Trava Save " + tabungan.getTujuan());
                        notif.setTerbaca(false);
                        notif.setUser(userTeman);
                        notifService.createNotif(notif);
                    }
                }
            }

        }

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(tabungan);
        baseResponse.setMessage("Tabungan Trava Save berhasil dibuat");

        return baseResponse;
    }

    @Transactional
    public BaseResponse saveTabungan64(Tabungan tabungan, UserPrincipal userPrincipal) {
        BaseResponse baseResponse = new BaseResponse();
        byte[] gambar = Base64.getMimeDecoder().decode(tabungan.getGambar_tabungan());

        Map uploadResult = cloudinaryConfig.upload(gambar,
                ObjectUtils.asMap("resourcetype", "auto"));

        tabungan.setGambar_tabungan(uploadResult.get("url").toString());

        User user = userRepository.findById(userPrincipal.getId())
                .orElseThrow();
        tabungan.setUser(user);
//        List<String> tab = tabungan.getNama();
//        List<String> rekT = tabungan.getRekening();
//
//        tab.add(user.getNamaLengkap());
//        rekT.add(user.getNoRekening());
//        tabungan.setNama(tab);
//        tabungan.setRekening(rekT);
        Period periode = Period.between(tabungan.getTarget(), LocalDate.now());
        Long bayar = (tabungan.getJumlah_tabungan() - tabungan.getSetoran_awal() * (tabungan.getRekening().size()));
        Long hari = ChronoUnit.DAYS.between(tabungan.getTarget(), LocalDate.now());
        int cicil = tabungan.getRekening().size();
        if (tabungan.getPeriode().toLowerCase().contains("harian")) {
            tabungan.setJumlah_setoran(bayar / ((-1 * hari) * cicil));
        } else if (tabungan.getPeriode().toLowerCase().contains("mingguan")) {
            tabungan.setJumlah_setoran(bayar / (((-1 * hari) / 7) * cicil));
        } else {
            tabungan.setJumlah_setoran(bayar / (((-1 * hari) / 30) * cicil));
        }
        Dana dana = danaRepository.findByUserId(user.getId());
        if (tabungan.getAutodebet()) {
            dana.setDana(dana.getDana().subtract(BigDecimal.valueOf(tabungan.getSetoran_awal())));
            tabungan.setTerkumpul(tabungan.getSetoran_awal());
            danaRepository.save(dana);
        } else {
            tabungan.setTerkumpul((long) 0);
        }
        tabunganRepository.save(tabungan);

        if (tabungan.getRekening() != null) {
            for (String rek : tabungan.getRekening()) {
                Tabungan tabunganTeman = new Tabungan();
                User userTeman = userRepository.findBynoRekening(rek);
                tabunganTeman.setTujuan(tabungan.getTujuan());
                tabunganTeman.setJumlah_tabungan(tabungan.getJumlah_tabungan());
                tabunganTeman.setGambar_tabungan(tabungan.getGambar_tabungan());
                tabunganTeman.setTarget(tabungan.getTarget());
                tabunganTeman.setSetoran_awal(tabungan.getSetoran_awal());
                tabunganTeman.setAutodebet(tabungan.getAutodebet());
                tabunganTeman.setPeriode(tabungan.getPeriode());
                tabunganTeman.setJumlah_setoran(tabungan.getJumlah_setoran());
                tabunganTeman.setTerkumpul(tabungan.getTerkumpul());
                tabunganTeman.setUser(userTeman);
                tabunganRepository.save(tabunganTeman);


                if (rek != user.getNoRekening()) {
                    Notif notif = new Notif();
                    notif.setJudul("Permintaan Gabung");
                    notif.setJenis("Travasave");
                    notif.setPesan(user.getNamaLengkap() + " mengundang kamu ke dalam Trava Save " + tabungan.getTujuan());
                    notif.setTerbaca(false);
                    notif.setUser(userTeman);
                    notifService.createNotif(notif);
                } else {
                    tabungan.setCreatedAt(tabunganTeman.getCreatedAt());
                    tabungan.setUpdatedAt(tabunganTeman.getUpdatedAt());
                    tabungan.setId(tabunganTeman.getId());
                }
            }
        }

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(tabungan);
        baseResponse.setMessage("Tabungan Trava Save berhasil dibuat");

        return baseResponse;
    }

    @Override
    public BaseResponse bayarManual(Long idTabungan) {
        BaseResponse baseResponse = new BaseResponse();
        Tabungan tabungan = tabunganRepository.findById(idTabungan)
                .orElseThrow();
        Long mulamula = tabungan.getTerkumpul();
        tabungan.setTerkumpul(mulamula + tabungan.getJumlah_setoran());
        Dana dana = danaRepository.findByUserId(tabungan.getUser().getId());
        BigDecimal saldo = dana.getDana();
        dana.setDana(saldo.subtract(BigDecimal.valueOf(tabungan.getJumlah_setoran())));
        danaRepository.save(dana);
        tabunganRepository.save(tabungan);

        Notif notif = new Notif();
        notif.setJudul("Top Up Berhasil");
        notif.setPesan("Top Up Trava Save " + tabungan.getTujuan() + " sebesar Rp." + tabungan.getJumlah_setoran() + " berhasil.");
        notif.setUser(tabungan.getUser());
        notif.setTerbaca(false);
        notif.setJenis("Travasave");
        notifService.createNotif(notif);

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(tabungan);
        baseResponse.setMessage("Pembayaran berhasil");
        return baseResponse;
    }

    @Override
    public TemanDTO tambahTeman(String rekening) {
        try {
            TemanDTO temanDTO = new TemanDTO();
            User user = userRepository.findBynoRekening(rekening);
            temanDTO.setNama(user.getNamaLengkap());
            temanDTO.setRekening(user.getNoRekening());
            return temanDTO;
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Transactional
    public BaseResponse editById(Long id, Tabungan newTabungan, MultipartFile gambar_tabungan) {
        BaseResponse baseResponse = new BaseResponse();

        Tabungan tabungan = tabunganRepository.findById(id)
                .orElseThrow(() ->
                        new DataNotFoundException(id));
        try {
            Map uploadResult = cloudinaryConfig.upload(gambar_tabungan.getBytes(),
                    ObjectUtils.asMap("resourcetype", "auto"));

            tabungan.setGambar_tabungan(uploadResult.get("url").toString());

        } catch (IOException e) {
            return new BaseResponse
                    (HttpStatus.BAD_REQUEST,
                            null,
                            "Upload foto gagal");
        }
        if (newTabungan.getTujuan() != null) {
            tabungan.setTujuan(newTabungan.getTujuan());
        }
        if (newTabungan.getJumlah_tabungan() != null) {
            tabungan.setJumlah_tabungan(newTabungan.getJumlah_tabungan());
        }
        if (newTabungan.getTarget() != null) {
            tabungan.setTarget(newTabungan.getTarget());
        }

        tabunganRepository.save(tabungan);

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(tabungan);
        baseResponse.setMessage("Data with id : " + tabungan.getId() + "is updated");
        return baseResponse;
    }

    @Transactional
    public ResponseEntity<?> dropSave(Long id) {
        return tabunganRepository.findById(id)
                .map(save -> {
                    tabunganRepository.delete(save);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new DataNotFoundException(id));
    }

    @Override
    public BaseResponse pendingTabungan(UserPrincipal userPrincipal, String status) {

        return null;
    }

    @Override
    public BaseResponse batalTabungan(UserPrincipal userPrincipal, String status) {
        return null;
    }

    @Transactional
    public BaseResponse findAll() {
        BaseResponse baseResponse = new BaseResponse();
        List<Tabungan> tabunganList = tabunganRepository.findAll();
        List<DaftarTabunganDTO> daftarTabunganDTOList = new ArrayList<>();
        DaftarTabunganDTO daftarTabunganDTO = new DaftarTabunganDTO();
        for (Tabungan tabungan : tabunganList) {
            daftarTabunganDTO.setId(tabungan.getId());
            daftarTabunganDTO.setNama_nasabah(tabungan.getUser().getNamaLengkap());
            daftarTabunganDTO.setJudul_tabungan(tabungan.getTujuan());
            daftarTabunganDTO.setNomor_rekening(tabungan.getUser().getNoRekening());
            daftarTabunganDTO.setSetoran(tabungan.getJumlah_setoran());
            daftarTabunganDTO.setTarget(tabungan.getJumlah_tabungan());
            daftarTabunganDTO.setTerkumpul(tabungan.getTerkumpul());
            daftarTabunganDTO.setStatus(tabungan.getStatus());
            daftarTabunganDTOList.add(daftarTabunganDTO);
        }

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(daftarTabunganDTOList);
        baseResponse.setMessage("Success");
        return baseResponse;
    }

    @Override
    public BaseResponse findUserAll(UserPrincipal userPrincipal) {
        BaseResponse baseResponse = new BaseResponse();
        List<Tabungan> tabunganList = tabunganRepository.findAllByUserIdOrderByCreatedAtDesc(userPrincipal.getId());
        baseResponse.setMessage("Data tabungan user dengan id " + userPrincipal.getId() + " berhasil diambil");
        baseResponse.setData(tabunganList);
        baseResponse.setStatus(HttpStatus.OK);
        return baseResponse;
    }

    @Transactional
    public BaseResponse findById(Long id) {
        BaseResponse baseResponse = new BaseResponse();
        Tabungan tabungan = tabunganRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(id));

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(tabungan);
        baseResponse.setMessage("Tabungan dengan " + id + " telah berhasil");
        return baseResponse;
    }


}
