package com.travada.backend.module.notif.repository;

import com.travada.backend.module.notif.model.Notif;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;
import java.util.Optional;

@Repository
public interface NotifRepository extends JpaRepository<Notif,Long> {

    List<Notif> findAllByUserIdOrderByCreatedAtDesc(Long id);
}
