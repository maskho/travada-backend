package com.travada.backend.module.tabungan.service;

import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.tabungan.model.Tabungan;
import com.travada.backend.module.tabungan.model.TemanDTO;
import com.travada.backend.module.user.model.User;
import com.travada.backend.utils.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public interface TabunganService {
    BaseResponse saveTabungan (Tabungan tabungan, UserPrincipal userPrincipal, MultipartFile gambar_tabungan);

    BaseResponse saveTabungan64 (Tabungan tabungan, UserPrincipal userPrincipal);


    BaseResponse bayarManual( Long idTabungan);

    TemanDTO tambahTeman(String rekening);

    BaseResponse editById(Long id, Tabungan tabungan, MultipartFile gambar_tabungan);

    BaseResponse findAll();

    BaseResponse findUserAll(UserPrincipal userPrincipal);

    BaseResponse findById(Long id);

    ResponseEntity<?> dropSave(Long id);

    BaseResponse pendingTabungan(UserPrincipal userPrincipal, String status);

    BaseResponse batalTabungan(UserPrincipal userPrincipal, String status);

}
