package com.travada.backend.module.booking.model.DTO;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.travada.backend.module.booking.model.Cicilan;
import com.travada.backend.module.booking.model.Pemesan;
import com.travada.backend.module.booking.model.Pemesanan;
import com.travada.backend.module.trip.model.Destinasi;
import com.travada.backend.module.user.model.User;
import lombok.Data;

import java.util.List;

@Data
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DetailPemesananDTO {
    private Pemesanan pemesanan;
    //    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    //    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private DestinasiDTO destinasi;
    private List<Cicilan> cicilan;
    private List<Pemesan> pemesan;
    private UserDTO user;
}
