package com.travada.backend.module.banking.model;

import lombok.Data;

@Data
public class TransaksikuDTO {
    private String nominal;
    private String rekening;
    private String pin;
}
