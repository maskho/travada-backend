package com.travada.backend.module.booking.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.banking.model.Banking;
import com.travada.backend.module.banking.model.Dana;
import com.travada.backend.module.banking.repository.BankingRepository;
import com.travada.backend.module.banking.repository.DanaRepository;
import com.travada.backend.module.banking.service.BankingService;
import com.travada.backend.module.booking.model.Cicilan;
import com.travada.backend.module.booking.service.CicilanService;
import com.travada.backend.module.transaksi.dto.TransferDto;
import com.travada.backend.module.transaksi.service.TransaksiService;
import com.travada.backend.module.user.repository.UserRepository;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
@RequestMapping("/cicilan")
public class CicilanController {
    @Autowired
    private DanaRepository danaRepository;
    @Autowired
    private CicilanService cicilanService;
    @Autowired
    private BankingRepository bankingRepository;

    @Autowired
    private BankingService bankingService;

    @Operation(summary = "Mengambil data cicilan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data cicilan berhasil diambil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data cicilan tidak berhasil diambil", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/{idDestinasi}/{orang}")
    private BaseResponse getCicilan(@PathVariable Long idDestinasi, @PathVariable int orang, @RequestParam(value = "berangkat") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate berangkat){
        BaseResponse baseResponse = new BaseResponse();

        baseResponse.setStatus(HttpStatus.CREATED);
        baseResponse.setData(cicilanService.viewCicilan(idDestinasi,orang,berangkat));
        baseResponse.setMessage("sukses");
        return baseResponse;
    }


    @Operation(summary = "Memperbaharui data cicilan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data cicilan berhasil diperbaharui", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data cicilan tidak berhasil diperbaharui", content = {@Content(mediaType = "application/json")})
    })
    @PutMapping("/{id}")
    private BaseResponse updateCicilan(@PathVariable Long id, @RequestParam String status){

        BaseResponse baseResponse = new BaseResponse();

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(cicilanService.updateCicilanById(id,status));
        baseResponse.setMessage("status sukses diupdate");
        return baseResponse;
    }

    @Operation(summary = "Memperbaharui data pembayaran cicilan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data pembayaran cicilan berhasil diperbaharui", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data pembayaran cicilan tidak berhasil diperbaharui", content = {@Content(mediaType = "application/json")})
    })
    @PutMapping("/bayar/{idCicilan}")
    public BaseResponse updateBayarCicilan(@CurrentUser UserPrincipal user, @PathVariable Long idCicilan){
        BaseResponse baseResponse = new BaseResponse();
        Banking banking = new Banking();
        Cicilan cicilan = cicilanService.updateCicilanById(idCicilan,"Dibayar");
//        TransferDto transferDto = new TransferDto();
//        transferDto.setAmount(cicilan.getJumlah());
//        //watch
//        transferDto.setDestination("rekening travada");
        Dana dana = danaRepository.findByUserId(user.getId());
        BigDecimal bayar = dana.getDana();
        dana.setDana(bayar.subtract(BigDecimal.valueOf(Math.abs(cicilan.getJumlah()))));
        danaRepository.save(dana);

        banking.setNama_asal("Saldo Aktif");
        banking.setNama_tujuan("Trava Plan");
        banking.setBank_asal("Travada");
        banking.setBank_tujuan("Travada");
        banking.setCatatan("Bayar cicilan "+cicilan.getPemesanan().getDestinasi().getNama_trip());
        banking.setRekening_asal(cicilan.getPemesanan().getUser().getNoRekening());
        banking.setRekening_tujuan("08157642696");
        banking.setNominal(String.valueOf(cicilan.getJumlah()));
        banking.setUser(cicilan.getPemesanan().getUser());
        bankingRepository.save(banking);
        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(cicilan);
        baseResponse.setMessage("Pembayaran cicilan dengan id "+idCicilan+" telah diterima");
        return baseResponse;
    }
}
