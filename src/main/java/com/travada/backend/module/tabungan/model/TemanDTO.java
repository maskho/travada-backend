package com.travada.backend.module.tabungan.model;

import lombok.Data;

@Data
public class TemanDTO {
    private String nama;
    private String rekening;
}
