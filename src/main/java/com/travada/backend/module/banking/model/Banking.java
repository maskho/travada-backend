package com.travada.backend.module.banking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.travada.backend.module.user.model.User;
import com.travada.backend.utils.AuditModel;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name="banking")
@Data
public class Banking extends AuditModel {
    @Id @GeneratedValue(generator = "generator_banking")
    @SequenceGenerator(
            name = "generator_banking",
            sequenceName = "sequence_banking",
            initialValue = 7000
    )
    private Long id;

    private String nama_asal;
    private String nama_tujuan;
    private String bank_asal;
    private String rekening_asal;
    private String bank_tujuan;
    private String rekening_tujuan;

    private String nominal;
    private String catatan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user;
}
