package com.travada.backend.module.user.controller;

import com.travada.backend.config.security.CurrentUser;
import com.travada.backend.config.security.UserPrincipal;
import com.travada.backend.module.user.dto.*;
import com.travada.backend.module.user.service.UserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/auth")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @Operation(summary = "Membuat user baru")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "user baru berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "user baru tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    //Create New User
    @PostMapping("/register")
    public ResponseEntity<?> createNewUser(@ModelAttribute CreateUserDto createUser,
                                           @RequestParam(value = "foto_ktp") MultipartFile foto_ktp,
                                           @RequestParam(value = "selfie_ktp") MultipartFile selfie_ktp) {

        return userService.createNewUser(createUser, foto_ktp, selfie_ktp);

    }

    @Operation(summary = "Cek registrasi user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "registrasi user berhasil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "registrasi user tidak berhasil", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping("/register/check")
    public ResponseEntity<?> checkRegistration(@RequestBody CheckRegisDto checkRegisDto) {

        return userService.checkRegistration(checkRegisDto);
    }

    @Operation(summary = "Konfirmasi user baru")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "konfirmasi user berhasil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "konfirmasi user tidak berhasil", content = {@Content(mediaType = "application/json")})
    })
    //Confirm New User
    @PostMapping("/confirm")
    public ResponseEntity<?> confirmNewUser(@RequestBody ConfirmationDto confirmUser) {

        return userService.confirmNewUser(confirmUser);
    }

    @Operation(summary = "Resend code via email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "resend code berhasil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "resend code tidak berhasil", content = {@Content(mediaType = "application/json")})
    })
    //Resend Code
    @PostMapping("/resend")
    public ResponseEntity<?> resendCode(@RequestBody ResendCodeDto resendCodeDto) {

        return userService.resendCode(resendCodeDto);
    }

    @Operation(summary = "Login User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "login user berhasil", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "login user tidak berhasil", content = {@Content(mediaType = "application/json")})
    })
    //Login
    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginDto loginDto) {

        return userService.authenticateUser(loginDto);
    }

    @Operation(summary = "Mengambil data akun user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    //Get my account
    @GetMapping("/user/me")
    public ResponseEntity<?> getMyAccount(@CurrentUser UserPrincipal userPrincipal) {

        return userService.getMyAccount(userPrincipal);
    }
}