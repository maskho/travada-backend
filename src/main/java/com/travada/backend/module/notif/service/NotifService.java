package com.travada.backend.module.notif.service;

import com.travada.backend.module.notif.model.Notif;
import com.travada.backend.utils.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public interface NotifService {
    BaseResponse getAll(Long idUser);

    BaseResponse getById(Long idNotif);

    Notif createNotif(Notif notif);

    ResponseEntity<?> dropNotif(Long id);
}
