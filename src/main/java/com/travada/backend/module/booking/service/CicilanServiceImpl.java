package com.travada.backend.module.booking.service;

import com.travada.backend.exception.DataNotFoundException;
import com.travada.backend.module.booking.model.Cicilan;
import com.travada.backend.module.booking.repository.CicilanRepository;
import com.travada.backend.module.booking.repository.PemesananRepository;
import com.travada.backend.module.trip.model.Destinasi;
import com.travada.backend.module.trip.repository.DestinasiRepository;
import com.travada.backend.utils.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Service("cicilanServiceImpl")
public class CicilanServiceImpl implements CicilanService {
    @Autowired
    private PemesananRepository pemesananRepository;

    @Autowired
    private DestinasiRepository destinasiRepository;

    @Autowired
    private CicilanRepository cicilanRepository;

    @Override
    public List<Cicilan> viewCicilan(Long idDestinasi, int orang, LocalDate berangkat) {
        Destinasi destinasi = destinasiRepository.findById(idDestinasi)
                .orElseThrow(() -> new DataNotFoundException(idDestinasi));

        if(berangkat == null){
            berangkat = destinasi.getBerangkat().get(0);
        }

        List<Cicilan> cicilanList = new ArrayList<>();
        Period periode = Period.between(berangkat, LocalDate.now());
        LocalDate jatuhTempo = LocalDate.now().plusMonths(1);

        while (jatuhTempo.isBefore(berangkat)) {
            Cicilan cicilan = new Cicilan();
            cicilan.setJatuh_tempo(jatuhTempo);
            cicilan.setJumlah((destinasi.getHarga_satuan()*orang) / (periode.getMonths() + (periode.getYears() * 12)));
            cicilan.setStatus("Menunggu Pembayaran");
            cicilanList.add(cicilan);
            jatuhTempo = jatuhTempo.plusMonths(1);
        }
        Cicilan cicilanAwal = cicilanList.get(0);
        Cicilan cicilanAkhir = cicilanList.get(cicilanList.size()-1);
        cicilanAwal.setJumlah((long) (cicilanAwal.getJumlah()+(cicilanAkhir.getJumlah()*0.5)));
        cicilanAkhir.setJumlah((long) (cicilanAkhir.getJumlah()-(cicilanAwal.getJumlah()*0.5)));

        return cicilanList;
    }

    @Override
    public List<Cicilan> createCicilan(Long idDestinasi, Long idPemesanan, int orang, LocalDate berangkat) {
        List<Cicilan> cicilanList = viewCicilan(idDestinasi, orang,berangkat);

        for(Cicilan cicilan: cicilanList){
            pemesananRepository.findById(idPemesanan).map(pemesanan -> {
                cicilan.setPemesanan(pemesanan);
                return cicilanRepository.save(cicilan);
            }).orElseThrow(()->new DataNotFoundException(idPemesanan));
        }
        return cicilanList;
    }

    @Override
    public List<Cicilan> getCicilan(Long idPemesanan) {
        return cicilanRepository.findAllByPemesananIdOrderByIdAsc(idPemesanan);
    }

    @Override
    public Cicilan updateCicilanById(Long id, String status) {
        Cicilan cicilan = cicilanRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(id));
        cicilan.setStatus(status);
        cicilanRepository.save(cicilan);
        return cicilan;
    }

    @Override
    public BaseResponse pembayaranCicilan(Long id) {
        BaseResponse baseResponse = new BaseResponse();

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(updateCicilanById(id,"Dibayar"));
        baseResponse.setMessage("Pembayaran cicilan dengan id "+id+" telah diterima");
        return baseResponse;
    }


    @Override
    public BaseResponse findCicilanById(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<?> dropById(Long id) {
        return null;
    }


}
