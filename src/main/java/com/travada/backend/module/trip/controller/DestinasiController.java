package com.travada.backend.module.trip.controller;

import com.travada.backend.module.trip.model.Destinasi;
import com.travada.backend.module.trip.service.DestinasiService;
import com.travada.backend.utils.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/destinasi")
public class DestinasiController {
    @Autowired
    private DestinasiService destinasiService;

    @Operation(summary = "Mengambil list data destinasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/all")
    public BaseResponse getAll() {
        return destinasiService.findAll();
    }

    @Operation(summary = "Mengambil list data destinasi berdasarkan popularitas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berdasarkan popularitas berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi berdasarkan popularitastidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/populer")
    public BaseResponse getPopuler() {
        return destinasiService.findAllSortByPopularitas();
    }

    @Operation(summary = "Mengambil list data destinasi berdasarkan pilihan")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berdasrkan pilihan berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi berdasarkan pilihan tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/pilihan")
    public BaseResponse getPilihan() {
        return destinasiService.findAllSortByPilihan();
    }

    @Operation(summary = "Mengambil list data destinasi berdasarkan harga di setiap benua")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data harga berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data harga tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/hargabenua")
    public BaseResponse getRangeHarga(@RequestParam Long termurah, @RequestParam Long termahal, @RequestParam String[] benua) {
        return destinasiService.findAllFilterHarga(termurah, termahal, benua);
    }

    @GetMapping("/pencarian")
    public BaseResponse getPencarian(@RequestParam String keyword) {
        if (keyword != "") {
            return destinasiService.findAllBySearch(keyword);
        }
        return destinasiService.findAll();
    }

    @Operation(summary = "Mengambil list data destinasi berdasarkan id destinasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    @GetMapping("/{id}")
    public BaseResponse getById(@PathVariable Long id) {
        return destinasiService.findById(id);
    }


    @Operation(summary = "Membuat destinasi baru")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "destinasi berhasil dibuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "destinasi tidak berhasil dibuat", content = {@Content(mediaType = "application/json")})
    })
    @PostMapping()
    public BaseResponse createDestinasi(@ModelAttribute Destinasi destinasi,
                                        @RequestParam MultipartFile[] foto) {
        return destinasiService.saveDestinasi(destinasi, foto);
    }

    @Operation(summary = "Memperbaharui data destinasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berhasil diperbaharui", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi tidak berhasil diperbaharui", content = {@Content(mediaType = "application/json")})
    })
    @PutMapping("/{id}")
    public BaseResponse putById(@PathVariable Long id, @ModelAttribute Destinasi destinasiReq, @RequestParam MultipartFile[] foto) {
        return destinasiService.editById(id, destinasiReq, foto);
    }

    @Operation(summary = "Menghapus data destinasi")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data destinasi berhasil dihapus", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data destinasi tidak berhasil dihapus", content = {@Content(mediaType = "application/json")})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<?> dropDestinasi(@PathVariable Long id) {
        return destinasiService.dropDestinasi(id);
    }
}
