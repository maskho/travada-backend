package com.travada.backend.module.banking.model;

import lombok.Data;

import java.util.Date;

@Data
public class TransaksiDTO {
    private Long id;
    private String nama_nasabah;
    private String deskripsi;
    private String jenis_transaksi;
    private Date tanggal;
    private String jumlah;
}
