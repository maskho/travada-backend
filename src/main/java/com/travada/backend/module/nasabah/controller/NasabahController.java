package com.travada.backend.module.nasabah.controller;

import com.travada.backend.module.nasabah.service.NasabahServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NasabahController {

    @Autowired
    private NasabahServiceImpl nasabahService;

    @Operation(summary = "Mengambil seluruh data nasabah")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data nasabah berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data nasabah tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    //Get All user
    @GetMapping("/nasabah")
    public ResponseEntity<?> getAllUser(
            @RequestParam(value = "status", required = false) String status) {

        return nasabahService.getAllUser(status);
    }

    @Operation(summary = "Mengambil detail data nasabah")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "detail data nasabah berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "detail data nasabah tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    //Get detail user
    @GetMapping("/nasabah/{id}")
    public ResponseEntity<?> getDetailUser(@PathVariable Long id) {

        return nasabahService.getDetailUser(id);
    }

    @Operation(summary = "Memperbaharui konfirmasi persetujuan nasabah")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "persetujuan nasabah berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "persetujuan nasabah tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    //Persetujuan nasabah
    @PutMapping("/nasabah/{id}")
    public ResponseEntity<?> confirmUserAccount(@PathVariable Long id,
                                                @RequestParam String status) {
        return nasabahService.acceptUserAccount(id, status);
    }

    @Operation(summary = "Mengambil seluruh data notifikasi nasabah ")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "data notifikasi nasabah berhasil dimuat", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "data notifikasi nasabah tidak berhasil dimuat", content = {@Content(mediaType = "application/json")})
    })
    //Get notification new user
    @GetMapping("/notifadmin")
    public ResponseEntity<?> getNewUserNotif() {
        return nasabahService.getNewUserNotif();
    }
}
