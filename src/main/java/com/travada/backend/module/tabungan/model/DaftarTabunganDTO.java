package com.travada.backend.module.tabungan.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DaftarTabunganDTO {
    private Long id;
    private String nama_nasabah;
    private String nomor_rekening;
    private String judul_tabungan;
    private Long setoran;
    private Long target;
    private Long terkumpul;
    private String status;
}
