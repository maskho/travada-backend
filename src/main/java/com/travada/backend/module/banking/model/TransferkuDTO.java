package com.travada.backend.module.banking.model;

import lombok.Data;

@Data
public class TransferkuDTO {
    private String rekeningTujuan;
    private String nominal;
    private String catatan;
    private String pin;
}
