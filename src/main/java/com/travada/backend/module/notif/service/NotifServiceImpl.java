package com.travada.backend.module.notif.service;

import com.travada.backend.exception.DataNotFoundException;
import com.travada.backend.module.booking.model.Pemesanan;
import com.travada.backend.module.notif.model.Notif;
import com.travada.backend.module.notif.repository.NotifRepository;
import com.travada.backend.utils.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("notifServiceImpl")
public class NotifServiceImpl implements NotifService{
    @Autowired
    private NotifRepository notifRepository;

    @Override
    public BaseResponse getAll(Long idUser) {
        BaseResponse baseResponse = new BaseResponse();
        List<Notif> notifList = notifRepository.findAllByUserIdOrderByCreatedAtDesc(idUser);

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(notifList);
        baseResponse.setMessage("Pengambilan notifikasi dengan idUser "+idUser+" berhasil dilakukan");

        return baseResponse;
    }

    @Override
    public BaseResponse getById(Long idNotif) {
        BaseResponse baseResponse = new BaseResponse();
        Notif notif = notifRepository.findById(idNotif)
                .orElseThrow(()->new DataNotFoundException(idNotif));

        notif.setTerbaca(true);
        notifRepository.save(notif);

//        if(notif.getJenis().contains("plan")){
//            Pemesanan pemesanan = notif.getPemesanan();
//            baseResponse.setData(pemesanan);
//        }
        //watch

        baseResponse.setStatus(HttpStatus.OK);
        baseResponse.setData(notif);
        baseResponse.setMessage("Pengambilan data notif dengan id "+idNotif+" berhasil dilakukan");
        return baseResponse;
    }

    @Override
    public Notif createNotif(Notif notif) {
        notifRepository.save(notif);
        return notif;
    }

    @Override
    public ResponseEntity<?> dropNotif(Long id) {
        return notifRepository.findById(id)
                .map(destinasi -> {
                    notifRepository.delete(destinasi);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new DataNotFoundException(id));
    }

}
